import Taro, {Component, Config} from '@tarojs/taro'
import {View, Swiper, SwiperItem, Image, Text,ScrollView} from '@tarojs/components'
import './transfer.scss'
import {AtGrid, AtCard, AtIcon, AtNavBar, AtList, AtListItem} from 'taro-ui'

export default class Index extends Component {

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '转账汇款'
  }

  constructor() {
    super(...arguments)
    this.state = {

    }
  }


  componentWillMount() {
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  componentDidShow() {

  }

  componentDidHide() {
  }

  render() {
    const params = this.$router.params;
    return (
      <View>
        <View className='at-row'>
          <View className='at-col-3 card-label'>
            <Text>卡号</Text>
          </View>
          <View className='at-col-7 item-center'>
            <Text>6235****5686/新借记卡</Text>
          </View>
          <View className='at-col-2 item-center'>
            <AtIcon value='chevron-right' size='16' color='#999'></AtIcon>
          </View>
        </View>
        <View className='row-tips'>
          账户余额<Text style='color:orange;'>194.30</Text>元
        </View>
        <View>我的名字是{params.name}</View>
      </View>

    )
  }
}

