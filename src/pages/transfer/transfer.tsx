import Taro, {Component, Config} from '@tarojs/taro'
import {View, Swiper, SwiperItem, Image, Text,ScrollView} from '@tarojs/components'
import './transfer.scss'
import {AtGrid, AtCard, AtIcon, AtNavBar, AtList, AtListItem} from 'taro-ui'

export default class Index extends Component {

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '转账汇款'
  }

  constructor() {
    super(...arguments)
    this.state = {
      current: 1,
      duration: 500,
      interval: 5000,
      isCircular: false,
      verticalIsCircular: false,
      isAutoplay: true,
      verticalIsAutoplay: false,
      hasIndicatorDots: true,
      verticalHasIndicatorDots: true,
      hotProduct: {
        title: '富利宝尊享计划(B18107)',
        tag: ['中低风险', '行内理财'],
        time: '募集期：2018.08.28-2018.08.30',
      }
    }
  }


  componentWillMount() {
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  componentDidShow() {
  }

  componentDidHide() {
  }

  startTransfer = (name) => {
    console.log(name);
    Taro.navigateTo({
      url: '../../pages/transfer/stepone?name='+name
    })
  }

  render() {
    return (
      <View>
        <View className='transfer-banner'>
          <View className='at-row transfer-tab'>
            <View className='at-col'>转账记录</View>
            <View className='at-col transfer-tab-right'>收款人名册</View>
          </View>
          <View style='width:100%;'>
            <Swiper
              className='transfer-swiper'
              slideMult='10'
              indicatorColor='#EEE'
              indicatorActiveColor='#bbb'
              current={0}
              duration={1000}
              interval={3000}
              circular={true}
              autoplay={false}
              indicatorDots={true}
              preMargin='20'>
              <SwiperItem>
                <View>
                  <View className='at-row'>
                    <View className='at-col item-center'>
                      <AtIcon value='repeat-play' size='45' color='#FFF'></AtIcon>
                      <Text style='margin-top:10px;'>本人账户互转</Text>
                    </View>
                    <View className='at-col item-center'>
                      <AtIcon value='money' size='45' color='#FFF'></AtIcon>
                      <Text style='margin-top:10px;'>转账</Text>
                    </View>
                    <View className='at-col item-center'>
                      <AtIcon value='bell' size='45' color='#FFF'></AtIcon>
                      <Text style='margin-top:10px;'>预约转账</Text>
                    </View>
                    <View className='at-col item-center'>
                      <AtIcon value='blocked' size='45' color='#FFF'></AtIcon>
                      <Text style='margin-top:10px;'>面对面转账</Text>
                    </View>
                  </View>
                </View>
              </SwiperItem>
              <SwiperItem>
                <View>
                  <View className='at-row'>
                    <View className='at-col item-center'>
                      <AtIcon value='repeat-play' size='45' color='#FFF'></AtIcon>
                      <Text style='margin-top:10px;'>本人账户互转</Text>
                    </View>
                    <View className='at-col item-center'>
                      <AtIcon value='money' size='45' color='#FFF'></AtIcon>
                      <Text style='margin-top:10px;'>转账</Text>
                    </View>
                    <View className='at-col item-center'>
                      <AtIcon value='bell' size='45' color='#FFF'></AtIcon>
                      <Text style='margin-top:10px;'>预约转账</Text>
                    </View>
                    <View className='at-col item-center'>
                      <AtIcon value='blocked' size='45' color='#FFF'></AtIcon>
                      <Text style='margin-top:10px;'>面对面转账</Text>
                    </View>
                  </View>
                </View>
              </SwiperItem>
            </Swiper>
          </View>
        </View>
        <ScrollView
                    scrollY
                    scrollWithAnimation
                    scrollTop='0'
                    style='height: 70VH;'
                    lowerThreshold='20'
                    upperThreshold='20'
        >
          <View className='divider'/>
          <View className='listTitle'>
            <Text className='bigTitle'>
              本人账户
            </Text>
            <Text className='tips'>
              点击这里给自己转账
            </Text>
          </View>
          <AtList>
            <AtListItem
              title='张三(8)'
              arrow='right'
              thumb='https://img12.360buyimg.com/jdphoto/s72x72_jfs/t6160/14/2008729947/2754/7d512a86/595c3aeeNa89ddf71.png'
            />
          </AtList>
          <View className='divider'/>
          <View className='listTitle'>
            <Text className='bigTitle'>
              常用收款人
            </Text>
            <Text className='tips'>
              最近转账的小伙伴 一点即转
            </Text>
          </View>
          <AtList>
            <AtListItem
              title='张某某01'
              note='鄞州银行(尾号0330)'
              onClick={this.startTransfer.bind(this,'张某某01')}
              thumb='https://img12.360buyimg.com/jdphoto/s72x72_jfs/t6160/14/2008729947/2754/7d512a86/595c3aeeNa89ddf71.png'
            />
            <AtListItem
              title='张某某02'
              note='鄞州银行(尾号0330)'
              onClick={this.startTransfer.bind(this,'张某某02')}
              thumb='https://img12.360buyimg.com/jdphoto/s72x72_jfs/t6160/14/2008729947/2754/7d512a86/595c3aeeNa89ddf71.png'
            />
            <AtListItem
              title='张某某'
              note='鄞州银行(尾号0330)'
              thumb='https://img12.360buyimg.com/jdphoto/s72x72_jfs/t6160/14/2008729947/2754/7d512a86/595c3aeeNa89ddf71.png'
            />
            <AtListItem
              title='张某某'
              note='鄞州银行(尾号0330)'
              thumb='https://img12.360buyimg.com/jdphoto/s72x72_jfs/t6160/14/2008729947/2754/7d512a86/595c3aeeNa89ddf71.png'
            />
            <AtListItem
              title='张某某'
              note='鄞州银行(尾号0330)'
              thumb='https://img12.360buyimg.com/jdphoto/s72x72_jfs/t6160/14/2008729947/2754/7d512a86/595c3aeeNa89ddf71.png'
            />
            <AtListItem
              title='张某某'
              note='鄞州银行(尾号0330)'
              thumb='https://img12.360buyimg.com/jdphoto/s72x72_jfs/t6160/14/2008729947/2754/7d512a86/595c3aeeNa89ddf71.png'
            />
          </AtList>
        </ScrollView>
      </View>

    )
  }
}

