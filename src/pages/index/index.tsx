import Taro, { Component, Config } from '@tarojs/taro'
import { View,Swiper,SwiperItem,Image,Text } from '@tarojs/components'
import './index.scss'
import { AtGrid,AtCard,AtIcon } from 'taro-ui'
import bannerOne from '../../assets/banner/banner4.png'
import bannerTwo from '../../assets/banner/banner5.png'
import bannerThree from '../../assets/banner/banner6.png'

export default class Index extends Component {

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '鄞州银行'
  }

  constructor () {
    super(...arguments)
    this.state = {
      current: 1,
      duration: 500,
      interval: 5000,
      isCircular: false,
      verticalIsCircular: false,
      isAutoplay: true,
      verticalIsAutoplay: false,
      hasIndicatorDots: true,
      verticalHasIndicatorDots: true,
      hotProduct:{
        title:'富利宝尊享计划(B18107)',
        tag:['中低风险','行内理财'],
        time:'募集期：2018.08.28-2018.08.30',
      }
    }
  }

  toTransfer = (item,index)=>{
    console.log("index:"+index);
    console.log(item);
    if(index=='01') {
      Taro.navigateTo({
        url: '../../pages/transfer/transfer'
      })
    }
  }

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    const {current,hotProduct}=this.state;
    return (
      <View className='index'>
        <Swiper
          slideMult='10'
          indicatorColor='#999'
          indicatorActiveColor='#333'
          current={current}
          duration={1000}
          interval={3000}
          circular={true}
          autoplay={true}
          indicatorDots={true}
          preMargin='20'>
          <SwiperItem>
            <View className='demo-text-1'>
              <Image
                style='width: 100%;height: 150px;'
                src={bannerOne} />
            </View>
          </SwiperItem>
          <SwiperItem>
            <View className='demo-text-2'>
              <Image
                style='width: 100%;height: 150px;'
                src={bannerTwo} />
            </View>
          </SwiperItem>
          <SwiperItem>
            <View className='demo-text-3'>
              <Image
                style='width: 100%;height: 150px;'
                src={bannerThree} />
            </View>
          </SwiperItem>
        </Swiper>
        <AtGrid data={
          [
            {
              icon:'list',
              value: '账户'
            },
            {
              image: 'https://img20.360buyimg.com/jdphoto/s72x72_jfs/t15151/308/1012305375/2300/536ee6ef/5a411466N040a074b.png',
              value: '转账汇款'

            },
            {
              image: 'https://img10.360buyimg.com/jdphoto/s72x72_jfs/t5872/209/5240187906/2872/8fa98cd/595c3b2aN4155b931.png',
              value: '借款'
            },
            {
              image: 'https://img12.360buyimg.com/jdphoto/s72x72_jfs/t10660/330/203667368/1672/801735d7/59c85643N31e68303.png',
              value: '信用卡'
            },
            {
              image: 'https://img14.360buyimg.com/jdphoto/s72x72_jfs/t17251/336/1311038817/3177/72595a07/5ac44618Na1db7b09.png',
              value: '生活缴费'
            },
            {
              image: 'https://img30.360buyimg.com/jdphoto/s72x72_jfs/t5770/97/5184449507/2423/294d5f95/595c3b4dNbc6bc95d.png',
              value: '充值中心'
            },
            {
              image: 'https://img10.360buyimg.com/jdphoto/s72x72_jfs/t5872/209/5240187906/2872/8fa98cd/595c3b2aN4155b931.png',
              value: '无卡取款'
            },
            {
              image: 'https://img30.360buyimg.com/jdphoto/s72x72_jfs/t5770/97/5184449507/2423/294d5f95/595c3b4dNbc6bc95d.png',
              value: '开心存款'
            },
            {
              image: 'https://img14.360buyimg.com/jdphoto/s72x72_jfs/t17251/336/1311038817/3177/72595a07/5ac44618Na1db7b09.png',
              value: '蜜钱包'
            },
            {
              image: 'https://img10.360buyimg.com/jdphoto/s72x72_jfs/t5872/209/5240187906/2872/8fa98cd/595c3b2aN4155b931.png',
              value: '全部功能'
            }
          ]
        }
                columnNum={5}
                onClick={this.toTransfer}
        >
        </AtGrid>
        <View className='divider'/>
        <AtCard
          isFull
          extra='更多'
          title='热销产品'
          thumb='http://img10.360buyimg.com/jdphoto/s72x72_jfs/t5872/209/5240187906/2872/8fa98cd/595c3b2aN4155b931.png'
        >
          <View className='at-row'>
            <View className='at-col at-col-4 at-col--auto'>
              <View className='incomeRate'>4.8%-4.9%</View>
              <View className='incomeTip'>预期年化</View>
            </View>
            <View className='at-col-7 prd-right'>
              <View className='prd-text'>
                {hotProduct.title}
              </View>
              <View>
                <Text className='prd-tag'>中低风险</Text>
                <Text className='prd-tag tag-right'>行内理财</Text>
              </View>
              <View className='prd-text'>
                {hotProduct.time}
              </View>
            </View>
            <View className='at-col at-col-1 at-col--auto icon-center'>
              <AtIcon value='chevron-right' size='30' color='#CCC'></AtIcon>
            </View>
          </View>
        </AtCard>
        <View className='divider'/>
        <AtCard
          isFull
          extra='更多'
          title='精选商品'
          thumb='http://img10.360buyimg.com/jdphoto/s72x72_jfs/t5872/209/5240187906/2872/8fa98cd/595c3b2aN4155b931.png'
        >
          <Swiper
            slideMult='10'
            indicatorColor='#999'
            indicatorActiveColor='#333'
            current={0}
            duration={1000}
            interval={3000}
            circular={true}
            autoplay={false}
            indicatorDots={true}
            preMargin='20'>
            <SwiperItem>
              <View >
                <View className='at-row'>
                  <View className='at-col item-center'>
                      <Image
                        style='width: 60px;height: 60px;'
                        src={bannerOne} />
                    <Text>南非西柚</Text>
                    <Text className='price'>¥59.00</Text>
                    <Text className='price-delete'>¥80.00</Text>
                  </View>
                  <View className='at-col item-center'>
                    <Image
                      style='width: 60px;height: 60px;'
                      src={bannerOne} />
                    <Text>石斛花小礼盒</Text>
                    <Text className='price'>¥58.00</Text>
                    <Text className='price-delete'>¥88.00</Text>
                  </View>
                  <View className='at-col item-center'>
                    <Image
                      style='width: 60px;height: 60px;'
                      src={bannerOne} />
                    <Text>小确幸</Text>
                    <Text className='price'>¥115.00</Text>
                    <Text className='price-delete'>¥134.00</Text>
                  </View>
                </View>
              </View>
            </SwiperItem>
            <SwiperItem>
              <View >
                <View className='at-row'>
                  <View className='at-col item-center'>
                    <Image
                      style='width: 60px;height: 60px;'
                      src={bannerThree} />
                    <Text>南非西柚</Text>
                    <Text className='price'>¥59.00</Text>
                    <Text className='price-delete'>¥80.00</Text>
                  </View>
                  <View className='at-col item-center'>
                    <Image
                      style='width: 60px;height: 60px;'
                      src={bannerThree} />
                    <Text>石斛花小礼盒</Text>
                    <Text className='price'>¥58.00</Text>
                    <Text className='price-delete'>¥88.00</Text>
                  </View>
                  <View className='at-col item-center'>
                    <Image
                      style='width: 60px;height: 60px;'
                      src={bannerThree} />
                    <Text>小确幸</Text>
                    <Text className='price'>¥115.00</Text>
                    <Text className='price-delete'>¥134.00</Text>
                  </View>
                </View>
              </View>
            </SwiperItem>
          </Swiper>
        </AtCard>
        <View className='divider'/>
        <AtCard
          isFull
          extra='鄞州区'
          title='附近网点'
          thumb='http://img10.360buyimg.com/jdphoto/s72x72_jfs/t5872/209/5240187906/2872/8fa98cd/595c3b2aN4155b931.png'
        >
          <View className='at-row'>
            <View className='at-col at-col-2 item-center'>
              <Image
                style='width: 60px;height:60px;'
                src={bannerThree} />
            </View>
            <View className='at-col-8 prd-right'>
              <View className='prd-text-title'>
                鄞州银行总行
              </View>
              <View className='prd-text'>
                浙江省宁波市鄞州区民惠西路88号
              </View>
              <View className='prd-text'>
                服务时间  8:30-16:30
              </View>
            </View>
            <View className='at-col at-col-2 clock-btn item-center'>
              <AtIcon value='clock' size='30' color='#02c476'></AtIcon>
              <View className='yy'>预约</View>
            </View>
          </View>
        </AtCard>
      </View>

    )
  }
}

